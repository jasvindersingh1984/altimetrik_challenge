Execute following command for program result
php index.php count 10
Complexity n*n

Execute following command for testcase execution
./vendor/bin/phpunit --bootstrap vendor/autoload.php test/Test

solution2.php if we have to maintain the data array then solution2 is best, this return result in less number of iterations
php solution2.php count 10
Complexity n/2*(n+1)