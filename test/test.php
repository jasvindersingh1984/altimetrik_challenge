<?php
declare(strict_types=1);
require_once('classes/Prime.php');
require_once('classes/Multiplication.php');
use PHPUnit\Framework\TestCase;


final class Test extends TestCase
{
    public function testCreatedForValidPrimeClassObject(): void
    {
        $primeObj = new Prime(10);
        $this->assertInstanceOf(Prime::class,$primeObj);
    }

    public function testInvalidArgumentType(): void
    {
        $this->expectException(TypeError::class);
        $primeObj = new Prime('invalid');
    }

    public function testForValidPrime(): void
    {
        $primeObj = new Prime(10);
        $primeObj->generateprimeNumbers();
        $this->assertEquals([2,3,5,7,11,13,17,19,23,29], $primeObj->primeNumbers);        
    }
    
    public function testFunctionReturnsNull(): void
    {   
        $primeObj = new Prime(10);        
        $this->assertNull($primeObj->generateprimeNumbers());
    }
    
    public function testInValidFunctionArguments(): void
    {
        $this->expectException(ArgumentCountError::class);
        $mulObj = new Multiplication();
        $mulObj->printValueArr();
    }
}