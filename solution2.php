<?php

declare(strict_types = 1);
require_once('classes/Prime.php');

if (isset($argv[2])) {
    $number = (int) $argv[2];
} else {
    if (!defined("STDIN")) {
        define("STDIN", fopen('php://stdin', 'rb'));
    }
    echo "Please enter count\n";
    $number = (int) fread(STDIN, 80);
}

if ($number === 0) {
    echo "Please enter valid count";
} else {
    try {
        $primeObj = new Prime($number);
        $primeObj->generateprimeNumbers();
        $res = [];
        foreach ($primeObj->primeNumbers as $key=>$row){
            for($i=$key;$i<10;$i++){
                $res[$primeObj->primeNumbers[$i]][$row]=$res[$row][$primeObj->primeNumbers[$i]]=$row*$primeObj->primeNumbers[$i];
            }
        }
        print_r($res);die;
    } 
    catch (Exception $e) {
        die("Exception occured " . $e->getMessage());
    }
}
echo "\n";





