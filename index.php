<?php

declare(strict_types = 1);
require_once('classes/Prime.php');
require_once('classes/Multiplication.php');

if (isset($argv[2])) {
    $number = (int) $argv[2];
} else {
    if (!defined("STDIN")) {
        define("STDIN", fopen('php://stdin', 'rb'));
    }
    echo "Please enter count\n";
    $number = (int) fread(STDIN, 80);
}

if ($number === 0) {
    echo "Please enter valid count";
} else {
    try {
        $primeObj = new Prime($number);
        $primeObj->generateprimeNumbers();
        $mulObj = new Multiplication();
        $mulObj->printValueArr($primeObj);
    } 
    catch (Exception $e) {
        die("Exception occured " . $e->getMessage());
    }
}
echo "\n";





