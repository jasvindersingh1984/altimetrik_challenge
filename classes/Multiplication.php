<?php
declare(strict_types=1);
class Multiplication {

    public function printValueArr(Prime $primeObj, int $indexRow = 0) {
        echo $primeObj->primeNumbers[$indexRow] . "\t|\t";
        $this->printValue($primeObj, $primeObj->primeNumbers[$indexRow]);
        $indexRow++;
        if ($primeObj->countofPrimeNumbers > $indexRow) {
            echo "\n";
            $this->printValueArr($primeObj, $indexRow);
        }
    }

    public function printValue(Prime $primeObj, int $multiplier, int $indexCol = 0) {
        echo $primeObj->primeNumbers[$indexCol] * $multiplier;
        $indexCol++;
        if ($primeObj->countofPrimeNumbers > $indexCol) {
            echo "\t";
            $this->printValue($primeObj, $multiplier, $indexCol);
        }
    }

}