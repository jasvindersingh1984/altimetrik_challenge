<?php
declare(strict_types=1);
class Prime {

    public $countofPrimeNumbers, $primeNumbers;

    public function __construct(int $countofPrimeNumbers) {
        $this->countofPrimeNumbers = $countofPrimeNumbers;
    }

    public function generateprimeNumbers() {
        echo "+\t|\t";
        $x = 0;
        $num = 2;
        while ($x < $this->countofPrimeNumbers) {
            $div_count = 0;
            for ($i = 1; $i <= $num; $i++) {
                if (($num % $i) == 0) {
                    $div_count++;
                }
            }
            if ($div_count < 3) {
                $this->primeNumbers[] = $num;
                echo $num . "\t";
                $x++;
            }
            $num++;
        }
        echo "\n";
    }

}